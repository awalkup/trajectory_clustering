README for the Trajectory Clustering
Author: Austin Walkup
Last Updated: April 9, 2012

####### kmeans_cluster.py #######

Performs a k-means clustering of a set of track files.

The input for the program is (* are optional arguments):
	The input directory (i.e. where the track files are stored), 
	output file base name (will be appended with _centers.csv, _labels.csv,
		 _stddeviation.csv). 
	*number of clusters (default = 10), 
	*output directory (where the output files are saved, default is 
		current directory).

The output is three files: 
	baseFileName_labels.csv which is what cluster each track belongs to,
	baseFileName_centers.csv which is the means/centers of the clusters,
	baseFileName_stddeviation.csv which is the standard deviation of the 
		data.

Note: 
This program requires scikits.learn to be installed and also must be in
your python path. On the department machines add
"/usr/local/scikits.learn/lib64/python2.7/site-packages" to your python path.

To run:
	% python kmeans_cluster.py <inputDir> <output_basename> [numClusters=10] [outputDir=.]



####### center_labeling.py #######

Opens up a shell interface that allows the user to label a centers file.

The input for the program is:
	A csv file that contains the cluster centers (with or without labels)
	An output file name which is where the data will be saved

The output for the program is:
	A csv file that contains all center data with labels (if any where added)

Note:
	* Commands for the shell are as follows:
		h or help : Displays the commands and what they do.
		q or quit : Quits the shell and saves the data.
		l or label <index> : Starts labeling procedure for a given index.
			If index is "all" then every center will be displayed in order.

To run:
	% python center_labeling.py <center_file.csv> <output_file>



####### traj_labeling.py #######

Labels a set of track files with a given, modified centers file.

The input for the program is:
	the input directory (i.e. where the track files are stored), 
	output directory (i.e. where the files are to be saved), 
	cluster center file (A modified center file where the last entry is the 
		label for the center)

The output is a prolog file for every input file:
	sampleFile.pl - which contains the relevant data (label, track number, 
		frame number, confidence) for the file

NOTES: 
	* If you run traj_labeling with less than two clusters in the centers
		file an error will occur.
	* If a trajectory vector is exactly the same as a center the label 
		will be 'invalid_label' with a confidence of 1, due to the 
		limitation of determining if a vector is completely invalid.

To run:
	% python traj_labeling.py <inputDir> <outputDir> <clusterCenterFile>



####### create_video.py #######

Creates a video from a subset of a given video and track file

The input for the program is the following (* are optional arguments):
	video name   - the name of the video file, 
	track name   - the name of the track file, 
	track number - the only track that would be in the video, 
	*track label - the label to assign to the track,
	*start frame - frame number in the track to start the video at,
	*end frame   - frame number in the track to end the video at

The output of the video is:
	A .mpg video file named the same as the original video file with the 
		track number and the start and end frames appended

Notes:
	* The program currently will save all of the frames from the video to a
		directory named almost the same as the video name, and afterwards 
		it combines the frames into a video
	* If the output video file already exists the program will wait for 
		conformation before overwriting the file

To run:
	% python create_video.py <videoName> <trackFile> <trackNumber> [trackLabel=''] [startFrame=-1] [endFrame=maxInt]
