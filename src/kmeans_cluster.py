#!/usr/bin/python2

# Austin Walkup
# Feb 07, 2012
#
# Takes one or more .trk file and computes trajectory vectors if there are 30+
# valid frames. Then all of the data will be clustered using k-means, and the 
# output will be three .csv files: 
# _centers.csv : contains the centers for the clusters
# _labels.csv : contains the cluster that each sample belongs to (in order)
# _stddeviation.csv : contains the standard deviation of each sample from it's assigned cluster center
#

#from pylab import *
from scikits.learn import cluster
from itertools import takewhile
import os, csv, sys, copy

MIN_TRAJ_LENGTH = 30 #The minimum number of valid tracks needed to make a traj vector

def usage(argv0):
	"""
	Displays how to use the program
	"""
	print'Usage:\n\t',argv0,'<inputDirectory> <outputFile> [numClusters=10] [outputDirectory=.]'
	exit(1)
#end usage

def outputCSV(data, filename, outputDir):
	"""
	Outputs data in csv format
	"""
	#opens the file for writing
	csvWriter = csv.writer(open(outputDir+'/'+filename+'.csv', 'wb'), delimiter=',')

	for row in data:
		try: #makes sure that row is iterable
			csvWriter.writerow(row)
		except csv.Error, e:
			csvWriter.writerow([row])
#end outputCSV

def stdDeviation(samples, labels, centers):
	"""
	Computes the standard deviation given the sample data, 
	the labels for what cluster each sample goes to, 
	and the cluster centers.
	"""
	print('computing standard deviation...')
	#creates a matrix of zeros
	stdDev = list(list(0 for i in range(len(centers[0])))for j in range(len(centers)))

	#creates a list of zeros
	numClusters = list(0 for i in range(len(centers)))

	#computes sum of squares
	for i in range(len(samples)):
		k = int(labels[i])
		numClusters[k]+=1
		for j in range(len(samples[0])):
			stdDev[k][j] += (samples[i][j] - centers[k][j])**2

	
	for i in range(len(stdDev)):
		for j in range(len(stdDev[0])):
			if numClusters[i] <= 1:
				continue
			
			stdDev[i][j] = round((stdDev[i][j] / (numClusters[i] - 1))**0.5, 10)
			
	return stdDev
#end stdDeviation

def makeTrajVector(trajData):
	"""
	Creates a trajectory vector from the frame data given
	"""
	mat = []
	vec = []
	for l in trajData:
		line = l.split()
		if len(line) <= 1:
			continue
		try:
			#frame is invalid
			if not int(line[-5]):
				del vec[:] #clears vec
				continue
			
			vec.append(float(line[-3])) #appends dx
			vec.append(float(line[-1])) #appends dy
			
			# checks if vec has reached the correct length
			if len(vec) >= 2*MIN_TRAJ_LENGTH:
				mat.append(copy.copy(vec))
				del vec[:] #clears vec
			
		except ValueError, e:
			print line, 'is not in the correct format'

	return mat
#end makeTrajVector

def checkLength(line):
	"""
	Checks that the last value on the Track line in the track file is less than
	MIN_TRAJ_LENGTH + 1
	"""
	try:
		return int(line.split()[-1]) > MIN_TRAJ_LENGTH + 1

	except:
		sys.stderr.write(' '.join(line),'is not in the correct format')
		sys.exit(1)
#end checkLength

def parseTrackFile(trackData):
	"""
	Parses a .trk file and returns a list of trajectory vectors, if any exist
	None otherwise
	"""
	trajMat = []
	for i in range(len(trackData)):
		line = trackData[i]
		if line.startswith('Track'):
			lineList = line.split()
			trackLen = int(lineList[lineList.index('Length')+1])
			if trackLen > MIN_TRAJ_LENGTH + 1:
				rest = takewhile(lambda x: not x.startswith('Track'), trackData[i+1:])
				trajVec = makeTrajVector(rest)
				if len(trajVec) != 0:
					trajMat.extend(trajVec)
	return trajMat
#end parseTrackFile

def getSamples(inputDir):
	"""
	Turns the data into a matrix of trajectory vectors
	"""
	print('getting input...')
	data = []
	for filename in os.listdir(inputDir):
		if not filename[filename.rfind('.'):] == '.trk':
			continue
		with open(inputDir+'/'+filename, 'rb') as f:
			temp = parseTrackFile(f.read().split('\n'))
			if len(temp) != 0:
				data.extend(temp)
	return data
#end getSamples

def kmeansClustering(samples, nclusters):
	"""
	Clusters the data using kmeans
	"""
	print('clustering...')
	return cluster.KMeans(init='k-means++', k=nclusters, n_init=10, verbose=0).fit(samples)
#end kmeansClustering

def main():
	"""
	Gets the arguments and passes them to the correct methods
	"""
	if len(sys.argv) < 3:
		usage(sys.argv[0])

	inputDir = sys.argv[1]
	outputFile = sys.argv[2]
	nclusters = 10;
	outputDir = '.'
	
	if len(sys.argv) > 3:
		nclusters = int(sys.argv[3])

	if len(sys.argv) > 4:
		outputDir = sys.argv[4]

	samples = getSamples(inputDir)
	km = kmeansClustering(samples, nclusters)

	outputCSV(km.labels_, outputFile+'_labels', outputDir)
	outputCSV(km.cluster_centers_, outputFile+'_centers', outputDir)

	stdDev = stdDeviation(samples, km.labels_, km.cluster_centers_)
	outputCSV(stdDev, outputFile+'_stddeviation', outputDir)

#end main

if __name__ == '__main__':
	main()
