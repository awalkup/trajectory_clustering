#!/usr/bin/python2
#
# Austin Walkup
# Feb 22, 2012
#
# Takes a video file, a track file, a track number, 
# and optional track label, start, and end frames
#

import cv2
import cv
import os, sys

def usage(argv0):
	"""
	Displays how to use the program then exits
	"""
	print 'Usage:\n\t',argv0,'<videoName> <trackFile> <trackNumber> [trackLabel] [startFrame] [endFrame]'
	exit(1)

#end usage

def writeFrames(videoName, videoDir, trackData, trackNum, trackLabel):
	"""
	Creates a video by saving all the frames and then combining them
	with a system call
	"""
	ZEROPAD = 4
	startStr = str(trackData[0][0])
	startStr = ('0'*(ZEROPAD - len(startStr))) + startStr

	endStr = str(trackData[-1][0])
	endStr = ('0'*(ZEROPAD - len(endStr))) + endStr

	start = int(startStr)
	end = int(endStr)

	padding = 30

	trackNumStr = str(trackNum)
	trackNumStr = ('0'*(ZEROPAD - len(trackNumStr))) + trackNumStr

	outFile = videoName[:videoName.rfind('.')]+'_'+trackNumStr+'_'+startStr+'-'+endStr
	os.system('mkdir ' + outFile)
	#outFile += '/'+outFile
	outFile += '/img'
	outExtension = '.jpg'

	vidFile = cv2.VideoCapture(videoDir+'/'+videoName)
	if not vidFile.isOpened():
		print'Could not open video',videoDir+'/'+videoName,'for reading'
		exit(1)

	success, img = vidFile.read()

	index = 0
	for index in range(0, start-padding):
		vidFile.read()

	i = 0
	while index < end + padding and success:	
		if index >= start and index < end:
			drawRect(img, trackData[index - start], trackLabel)

		indexStr = str(i)
		indexStr = ('0'*(ZEROPAD - len(indexStr))) + indexStr
		cv.SaveImage(outFile+indexStr+outExtension, cv.fromarray(img))

		success, img = vidFile.read()
		index += 1
		i += 1
	#end while

	#make video then clean up
	os.system('ffmpeg -f image2 -i '+outFile+'%0'+str(ZEROPAD)+'d'+outExtension+' '+outFile[:outFile.rfind('/')]+'.mpg')
	os.system('rm -rf '+outFile[:outFile.rfind('/')])
#end writeFrames

def drawRect(frame, rectData, trackLabel):
	"""
	Draws a rectangle on the frame
	"""
	frameNum, tlx, tly, width, height = map(int, rectData)

	tlx *= 2
	tly *= 2
	width *= 2
	height *= 2
	color = (0,255,255)

	cv2.putText(img=frame, text=trackLabel, org=(tlx,tly+height+15),fontFace=cv2.FONT_HERSHEY_SIMPLEX, fontScale=0.5, color=color)

	cv2.rectangle(img=frame, pt1=(tlx,tly), pt2=(tlx+width, tly+height), color=color, thickness=3)
#end drawRect

def makeVideo(videoName, videoDir, trackData, trackNum, trackLabel):
	"""
	Creates a video from start to end
	"""
	ZEROPAD = 4
	startStr = str(trackData[0][0])
	startStr = ('0'*(ZEROPAD - len(startStr))) + startStr

	endStr = str(trackData[-1][0])
	endStr = ('0'*(ZEROPAD - len(endStr))) + endStr

	start = int(startStr)
	end = int(endStr)

	padding = 30

	trackNumStr = str(trackNum)
	trackNumStr = ('0'*(ZEROPAD - len(trackNumStr))) + trackNumStr

	outFile = './'+videoName[:videoName.rfind('.')]+'_'+trackNumStr+'-'+startStr+'-'+endStr
	outFile += '.mov'

	vidFile = cv2.VideoCapture(videoDir+'/'+videoName)
	if not vidFile.isOpened():
		print'Could not open video',videoDir+'/'+videoName,'for reading'
		exit(1)

	success, img = vidFile.read()

	vidWidth, vidHeight, vidChannel = img.shape
	
	vidWriter = cv2.VideoWriter(outFile, fourcc=cv.CV_FOURCC('I','Y','U','V'), fps=30,frameSize=(vidWidth, vidHeight), isColor=vidChannel)

	index = 0
	for index in range(0, start-padding):
		vidFile.read()

	while index < end + padding and success:
		if index >= start and index < end:
			drawRect(img, trackData[index - start], trackLabel)

		vidWriter.write(img)

		success, img = vidFile.read()
		index += 1
	#end while
#end makeVideo

def parseFrame(frameData):
	"""
	Parses the frame data and returns only the important info
	the frame number, the x corrdinate, y coordinate, width, and height
	"""
	data = []
	for line in frameData:
		line = line.split()
		try:
			frameNum = int(line[1])
			xCoord = float(line[3])
			yCoord = float(line[5])
			width = float(line[7])
			height = float(line[9])

			data.append((frameNum, xCoord, yCoord, width, height))
		except ValueError, e:
			print ''.join(line),'in',trackFile,'is not in the right format'
			exit(1)

	return data
#end parseData

def parseTrackFile(trackFile, trackNum, startFrame, endFrame):
	"""
	Extracts the relevant data from the track file and returns it
	"""
	f = open(trackFile, 'rb').read().split('Track ')
	for track in f:
		if not track.startswith(str(trackNum)):
			continue

		track = track.split('\n')

		if startFrame == -1 and endFrame == sys.maxint:
			return parseFrame(track[1:-2]) #strips off the track line, and the newlines

		try:
			data = filter(lambda x: int(x.split()[1]) >= startFrame and int(x.split()[1]) < endFrame, track[1:-2])

		except ValueError, e:
			print 'Track',trackNum,'in',trackFile,'is not in the right format'
			exit(1)

	return parseFrame(data)
#end parseTrackFile

def main():
	"""
	Gets the command arguments, and calls the appropriate methods
	"""
	argLen = len(sys.argv)
	if argLen < 4 or argLen > 7:
		usage(sys.argv[0])

	videoDir = sys.argv[1]
	dirIndex = videoDir.rfind('/')

	if dirIndex == -1:
		videoName = videoDir
		videoDir = '.'
	else:
		videoName = videoDir[dirIndex+1:]
		videoDir = videoDir[:dirIndex]


	trackFile = sys.argv[2]
	trackLabel = ''
	startFrame = -1
	endFrame = sys.maxint
	try:
		trackNum = int(sys.argv[3])

		if argLen > 4:
			trackLabel = sys.argv[4]

		if argLen > 5:
			startFrame = int(sys.argv[5])

		if argLen > 6:
			endFrame = int(sys.argv[6])

	except ValueError, e:
		usage(sys.argv[0])

	trackData = parseTrackFile(trackFile, trackNum, startFrame, endFrame)
	if len(trackData) == 0:
		print '\nFrames',str(startFrame)+'-'+str(endFrame),'do not exist for track',trackNum,'in the file',trackFile
		exit(1)
	#makeVideo(videoName, videoDir, trackData, trackNum, trackLabel)
	writeFrames(videoName, videoDir, trackData, trackNum, trackLabel)
#end main

if __name__ == '__main__':
	main()
