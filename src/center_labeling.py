#!/usr/bin/python2

# Austin Walkup
# April 4, 2012
#
# Takes a centers file from kmeans_cluster.py and shows the user the 
# trajectory of the centers one by one and asks for labels.
# The output is a centers file with labels attached to the cluster centers
#
import scipy 
import matplotlib.pyplot as plt
import numpy as np
import time, csv, sys, os, math

def usage(argv0):
	"""
	How to use the program
	"""
	print 'Usage:\n\t',argv0,'<center_file.csv> <output_file>'
	exit(1)
#end usage

def graph_traj(center):
	"""
	Graphs the given center with matplotlib
	"""
	plt.ion()
	fig = plt.figure()
	ax = fig.add_subplot(111)
	ax.grid(True, which='both')

	x = [0]
	y = [0]
	
	length = len(center)

	try:
		int(center[-1]) #throws error if center is already labeled
	except:
		length -= 1

	[x.append(x[-1]+float(center[i])) if i % 2 == 0 else y.append(y[-1]+ \
								-1*float(center[i])) \
								for i in range(0, length)]

	xmax = max(x)
	xmin = min(x)
	ymax = max(y)
	ymin = min(y)

	tmax = max(xmax, ymax) + 2.0
	tmin = min(xmin, ymin) - 2.0

	ax.set_ylim(tmin, tmax)
	ax.set_xlim(tmin, tmax)

	tickmin = math.ceil((tmin/10))*10
	tickmax = math.ceil((tmax/10))*10

	ax.set_yticks(scipy.arange(tickmin, tickmax, 10))
	ax.set_xticks(scipy.arange(tickmin, tickmax, 10))

	for i in range(1, len(x)):
		time.sleep(0.25)
		ax.plot(x[:i], y[:i], color='black', marker='o')
		fig.canvas.draw()

#end graph_traj

def getLabel(center):
	"""
	Asks for the label of the given center
	"""
	while 1:
		try: # checks of the center is already labeled
			curr_label = float(center[-1])
			curr_label = None
		except:
			curr_label = center[-1]

		print 'current label:',curr_label
		print 'enter an appropriate label or r to replay:',
		label = raw_input()

		if label == 'r':
			graph_traj(center)
		elif len(label) < 1: #if they give no input
			break
		else:
			center.append(label)
			break

#end getLabel

def centerLabelTerminal(centers):
	"""
	Enters a shell where commands can be issued
	"""
	print 'Welcome to Center Labeling.\nFor a list of commands enter h',\
			'or help'
	parseCommand(centers)
#end commandParser

def printHelp():
	"""
	Displays the commands the can be given to the shell
	"""
	print '   h or help: display all commands and their function.\n'
	print '   q or quit: quits the program.\n'
	print '   l or label <index>: starts labeling procedure for a given '+\
			'index. If index is "all" every center will be displayed in '+\
			'order\n'
#end printHelp

def parseCommand(centers):
	"""
	Interprets the commands given
	"""
	invalidCmd = 'is not a valid command. Type h or help to get a full'+\
					' list of commands'

	while 1:
		try:
			print "center_label>",
			cmdIn = raw_input()
		
			if cmdIn == 'h' or cmdIn == 'help':
				printHelp()

			elif cmdIn == 'q' or cmdIn == 'quit':
				break

			elif cmdIn.startswith('label') or cmdIn.startswith('l'):
				indexstr = cmdIn.split(' ')[1]
				index = -1

				try:
					index = int(indexstr) - 1 
				except:
					pass

				if indexstr == 'all':
					for c in centers:
						graph_traj(c)
						getLabel(c)
					#end for
				elif index >= 0 or index < len(centers):
					graph_traj(centers[index])
					getLabel(centers[index])

				else:
					print 'Not a valid center index. Valid indices are 1 -'\
							,len(centers)
			#end label
			else:
				print cmdIn, invalidCmd
		except Exception, e:
			print e,'\n\n',cmdIn, invalidCmd
#end parseCommand

def readCenterFile(filename):
	"""
	Reads a csv file to get the centers info
	"""
	csvReader = csv.reader(open(sys.argv[1], 'rb'), delimiter=',')
	return [list(line) for line in csvReader]
#end readCenterFile

def saveCenters(centers, filename):
	"""
	Saves the centers that were labeled
	"""
	if len(centers) < 1:
		return

	csvWriter = csv.writer(open(filename,'wb'), delimiter=',')

	for c in centers:
		csvWriter.writerow(c)
#saveCenters

def main():
	"""
	Main controller of the program. Calls appropriate methods
	"""
	if len(sys.argv) < 3 or len(sys.argv) > 4:
		usage(sys.argv[0])

	inFile = sys.argv[1]
	outFile = sys.argv[2]

	centers = readCenterFile(inFile)
	centerLabelTerminal(centers)
	saveCenters(centers, outFile)
#end main

if __name__ == '__main__':
	main()
